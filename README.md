### Multimedia Metronome App

A free app that tells you the step per minute to go from where you are to the place that you have chosen in a determinated amount of time that the user have picked.

It is possible to link the app with your Spotify account (only premium) to choose a playlist and the app will automatically choose the song in this playlist to play that has the same, or similar, bpm as the step per minute calculated by the app.

A more detailed implementation can be found in the "Tesi_LT_Emanuele_Fazzini.pdf" file, in Italian.
