package com.example.multimediametronome2;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import com.example.multimediametronome2.initialsetup.InitialSetupFragment;

public class InitialActivity extends AppCompatActivity {
    Button next;
    Bundle sis;
    TextView textViewInitial;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_initial);
        callInitialFragment(savedInstanceState);
    }

    private void callInitialFragment(Bundle savedInstanceState) {
        FragmentManager fm = getSupportFragmentManager();
        if (findViewById(R.id.fragment_container) == null || savedInstanceState == null) {
            FragmentTransaction transaction = fm.beginTransaction();
            transaction.replace(R.id.fragment_container, new InitialSetupFragment());
            transaction.commit();
        }
    }
}
