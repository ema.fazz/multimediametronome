package com.example.multimediametronome2.database;

import androidx.room.RoomDatabase;

@androidx.room.Database(entities = {User.class, StepsLengthHistory.class}, version = 1)
public abstract class MyDatabase extends RoomDatabase {

    public abstract MyDBAccessObjectsUser mydao();
    public abstract MyDBAccessObjectsSH mydaoSH();

}
