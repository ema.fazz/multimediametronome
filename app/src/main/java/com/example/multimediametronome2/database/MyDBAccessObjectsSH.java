package com.example.multimediametronome2.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface MyDBAccessObjectsSH {

    @Insert
    public void addStep(StepsLengthHistory slh);

    @Query("select * from StepsLengthHistory")
    public List<StepsLengthHistory> getSLH();

}
