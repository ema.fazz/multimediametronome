package com.example.multimediametronome2.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface MyDBAccessObjectsUser {

    @Insert
    public void addUser(User u);

    @Query("select * from User")
    public List<User> getUser();

    @Query("delete from User")
    public void removeOldUser();


}
