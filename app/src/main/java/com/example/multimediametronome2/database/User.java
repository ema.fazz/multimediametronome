package com.example.multimediametronome2.database;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class User {

    @NonNull
    @PrimaryKey
    private String name;
    private int cmStep;

    public String getName() {
        return name;
    }

    public int getCmStep() {
        return cmStep;
    }

    public void setCmStep(int cmStep) {
        this.cmStep = cmStep;
    }

    public void setName(String name) {
        this.name = name;
    }


}
