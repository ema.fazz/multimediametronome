package com.example.multimediametronome2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class StopServiceReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        context.stopService(new Intent(context, MetronomeService.class));
    }
}
