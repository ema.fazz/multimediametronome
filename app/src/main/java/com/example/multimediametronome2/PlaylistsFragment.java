package com.example.multimediametronome2;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PlaylistsFragment extends Fragment {
    /* access modifiers changed from: private */
    public JSONArray playlists;

    /* renamed from: rg */
    private RadioGroup f65rg;

    /* renamed from: sp */
    private SharedPreferences f66sp;

    public PlaylistsFragment(JSONArray playlists2) {
        this.playlists = playlists2;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_playlists, container, false);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("prefs", 0);
        this.f66sp = sharedPreferences;
        int plSelected = sharedPreferences.getInt("plSelected", 0);
        final SharedPreferences.Editor editsp = this.f66sp.edit();
        this.f65rg = (RadioGroup) view.findViewById(R.id.radioPalylists);
        JSONArray jSONArray = this.playlists;
        if (jSONArray == null || jSONArray.length() <= 0) {
            ((TextView) view.findViewById(R.id.noPlFound)).setText("No results found");
        } else {
            for (int i = 0; i < this.playlists.length(); i++) {
                RadioButton rb = new RadioButton(getContext());
                try {
                    rb.setText(this.playlists.getJSONObject(i).getString("name"));
                    rb.setId(i);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                this.f65rg.addView(rb);
            }
            this.f65rg.check(plSelected);
            this.f65rg.setOrientation(1);
            this.f65rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    RadioButton radioButton = (RadioButton) view.findViewById(checkedId);
                    String plid = "";
                    for (int i = 0; i < PlaylistsFragment.this.playlists.length(); i++) {
                        try {
                            JSONObject pl = PlaylistsFragment.this.playlists.getJSONObject(i);
                            if (radioButton.getText().toString().equals(pl.getString("name"))) {
                                plid = pl.getString("id");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    editsp.putString("plIDSelected", plid);
                    editsp.putInt("plSelected", checkedId);
                    editsp.apply();
                    Log.d("", radioButton.getText() + "  " + plid);
                }
            });
        }
        return view;
    }
}
