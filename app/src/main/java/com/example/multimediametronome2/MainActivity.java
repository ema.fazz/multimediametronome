package com.example.multimediametronome2;

import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.room.Room;
import com.example.multimediametronome2.database.MyDatabase;
import com.example.multimediametronome2.database.StepsLengthHistory;
import com.example.multimediametronome2.database.User;
import com.example.multimediametronome2.directionhelpers.TaskLoadedCallback;
import com.example.multimediametronome2.initialsetup.InitialNameFragment;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.navigation.NavigationView;
import com.spotify.android.appremote.api.SpotifyAppRemote;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, TaskLoadedCallback {
    private static final String CHANNEL_ID = "NotificationServiceChannel";
    private static final String CLIENT_ID = "383a3374409d4589bb7e03658c38ef15";
    private static final String ENDPOINT = "https://api.spotify.com/v1/me/playlists";
    private static final int INTERNET_ACTIVITY = 40;
    private static final int LOCATION_REQUEST = 30;
    private static final int PHYISCAL_ACTIVITY = 50;
    private static final String REDIRECT_URI = "multimedia-metronome://callback";
    private static final int REQUEST_CODE = 1337;
    public static MyDatabase mydb;
    private static String token;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private Polyline currentPolyline;
    private DrawerLayout drawerLayout;
    private int indexLastFragment;
    private GoogleMap mMap;
    private SpotifyAppRemote mSpotifyAppRemote;
    /* access modifiers changed from: private */
    public MapsFragment mapFragment;
    private NavigationView navigationView;
    private boolean permissionGranted;
    /* access modifiers changed from: private */
    public JSONArray playlists;
    private SharedPreferences prefs;
    /* access modifiers changed from: private */
    public List<StepsLengthHistory> slhList;
    /* access modifiers changed from: private */
    public String spotifyPlaylists;
    /* access modifiers changed from: private */
    public boolean steptook;
    private Toolbar toolbar;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_main);
        mydb = Room.databaseBuilder(getApplicationContext(), MyDatabase.class, "User").build();
        SharedPreferences sharedPreferences = getSharedPreferences("prefs", 0);
        this.prefs = sharedPreferences;
        this.steptook = sharedPreferences.getBoolean("stepLengthTook", true);
        if (Boolean.valueOf(this.prefs.getBoolean("init", true)).booleanValue()) {
            callInitialSetup();
        } else {
            new AsyncTaskShowUser().execute(new Void[0]);
        }
        /*if (ContextCompat.checkSelfPermission(this, "android.permission.ACTIVITY_RECOGNITION") != 0) {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.ACTIVITY_RECOGNITION"}, 50);
        }
        if (ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") != 0) {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.ACCESS_FINE_LOCATION"}, 30);
        }*/
        if (ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") == 0) {
            getSupportFragmentManager().beginTransaction().replace((int) R.id.fragment_container, (Fragment) new MapsFragment(), "MapFragment").addToBackStack("MapFragment").commit();
        }
        Toolbar toolbar2 = (Toolbar) findViewById(R.id.toolbar);
        this.toolbar = toolbar2;
        toolbar2.setTitle((CharSequence) "BPMWALKER");
        setSupportActionBar(this.toolbar);
        this.drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        this.navigationView = (NavigationView) findViewById(R.id.nav_view);
        ActionBarDrawerToggle actionBarDrawerToggle2 = new ActionBarDrawerToggle(this, this.drawerLayout, this.toolbar, R.string.open, R.string.close);
        this.actionBarDrawerToggle = actionBarDrawerToggle2;
        this.drawerLayout.addDrawerListener(actionBarDrawerToggle2);
        this.actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        this.actionBarDrawerToggle.syncState();
        this.navigationView.setNavigationItemSelectedListener(this);
        AuthenticationRequest.Builder builder = new AuthenticationRequest.Builder(CLIENT_ID, AuthenticationResponse.Type.TOKEN, REDIRECT_URI);
        builder.setScopes(new String[]{"streaming"});
        AuthenticationClient.openLoginActivity(this, REQUEST_CODE, builder.build());
        this.navigationView.setCheckedItem((int) R.id.home_nav);
        createNotificationChannel();
    }

    @Override
    protected void onStart() {

        super.onStart();
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode != 30) {
            if (requestCode == 50) {
                if (grantResults.length <= 0 || grantResults[0] != 0) {
                    permissionNotGranted();
                } else if (ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") != 0) {
                    ActivityCompat.requestPermissions(this, new String[]{"android.permission.ACCESS_FINE_LOCATION"}, 30);
                }
            }
        } else if (grantResults.length <= 0 || grantResults[0] != 0) {
            permissionNotGranted();
        } else {
            getSupportFragmentManager().beginTransaction().replace((int) R.id.fragment_container, (Fragment) new MapsFragment(), "MapFragment").addToBackStack("MapFragment").commit();
            this.mapFragment = (MapsFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        }
    }

    public void permissionNotGranted() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("I permessi sono necessari");
        alertDialog.setMessage("Senza i permessi l'app non riesce ad accedere alle sue funzionalità");
        alertDialog.setPositiveButton("Chiudi", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                MainActivity.this.startActivity(new Intent(MainActivity.this.getApplicationContext(), MainActivity.class));
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }

    private double variance(List<StepsLengthHistory> a, int n) {
        double sum = 0.0d;
        for (int i = 0; i < n; i++) {
            sum += (double) a.get(i).getStepLength();
        }
        double mean = sum / ((double) n);
        double sqDiff = 0.0d;
        for (int i2 = 0; i2 < n; i2++) {
            sqDiff += (((double) a.get(i2).getStepLength()) - mean) * (((double) a.get(i2).getStepLength()) - mean);
        }
        return sqDiff / ((double) n);
    }

    /* access modifiers changed from: private */
    public double standardDeviation(List<StepsLengthHistory> arr, int n) {
        return Math.sqrt(variance(arr, n));
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == REQUEST_CODE) {
            AuthenticationResponse response = AuthenticationClient.getResponse(resultCode, intent);
            int i = C09322.f59x15f9af85[response.getType().ordinal()];
            if (i == 1) {
                token = response.getAccessToken();
                String plid = this.prefs.getString("plIDSelected", "none");
                SharedPreferences.Editor editprefs = this.prefs.edit();
                editprefs.putString("tokenSpotify", token);
                if (plid.equals("none")) {
                    editprefs.putString("plIDSelected", "37i9dQZEVXbMDoHDwVN2tF");
                }
                editprefs.apply();
                new JsonTask().execute(new String[]{ENDPOINT});
                Log.d("ENTRATOINTOKEN", "ENTRATOOOOOOO   " + token);
            } else if (i == 2) {
                Log.d("ENTRATOINERROR", "" + response.getError());
            }
        }
    }

    /* renamed from: com.example.multimediametronome2.MainActivity$2 */
    static /* synthetic */ class C09322 {

        /* renamed from: $SwitchMap$com$spotify$sdk$android$authentication$AuthenticationResponse$Type */
        static final /* synthetic */ int[] f59x15f9af85;

        static {
            int[] iArr = new int[AuthenticationResponse.Type.values().length];
            f59x15f9af85 = iArr;
            try {
                iArr[AuthenticationResponse.Type.TOKEN.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f59x15f9af85[AuthenticationResponse.Type.ERROR.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        new AsyncTaskShowUser().execute(new Void[0]);
        if (this.steptook) {
            this.indexLastFragment = getSupportFragmentManager().getBackStackEntryCount() - 1;
        }
        Boolean serviceIsRunning = Boolean.valueOf(this.prefs.getBoolean("serviceIsStart", false));
        if (getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof MapsFragment) {
            this.mapFragment = (MapsFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        }
        Log.d("ONRESUMESERVICEBOOL", String.valueOf(serviceIsRunning));
        if (getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof MapsFragment) {
            this.mapFragment = (MapsFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        }
        if (!(getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof MapsFragment)) {
            super.onResume();
        } else if (!serviceIsRunning.booleanValue()) {
            super.onResume();
            this.mapFragment.resetImageStartView();
        } else {
            super.onResume();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this, MetronomeService.class));
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= 26) {
            ((NotificationManager) getSystemService(NotificationManager.class)).createNotificationChannel(new NotificationChannel(CHANNEL_ID, "Service Manager", NotificationManager.IMPORTANCE_DEFAULT));
        }
    }

    public void onBackPressed() {
        this.indexLastFragment = getSupportFragmentManager().getBackStackEntryCount() - 1;
        Boolean serviceIsRunning = Boolean.valueOf(this.prefs.getBoolean("serviceIsStart", false));
        if (getSupportFragmentManager().getBackStackEntryAt(this.indexLastFragment).getName().equals("MapFragment")) {
            if (serviceIsRunning.booleanValue()) {
                Intent startMain = new Intent("android.intent.action.MAIN");
                startMain.addCategory("android.intent.category.HOME");
                startMain.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(startMain);
            } else if (this.currentPolyline != null) {
                this.mMap.clear();
                this.currentPolyline = null;
                this.mapFragment.setMarkerOptions((MarkerOptions) null);
                this.mapFragment.resetImageStartView();
                this.mapFragment.setStartRouteVisibility(8);
            } else {
                Intent startMain2 = new Intent("android.intent.action.MAIN");
                startMain2.addCategory("android.intent.category.HOME");
                startMain2.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(startMain2);
            }
        } else if (getFragmentManager().getBackStackEntryCount() > 1) {
            getFragmentManager().popBackStack();
        } else if (this.indexLastFragment + 1 < 2) {
            Intent startMain3 = new Intent("android.intent.action.MAIN");
            startMain3.addCategory("android.intent.category.HOME");
            startMain3.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(startMain3);
        } else {
            super.onBackPressed();
        }
    }

    private void callInitialSetup() {
        startActivity(new Intent(this, InitialActivity.class));
        finish();
    }

    public static String actionJSONFromURLConnection(String urlString, String action) {
        HttpURLConnection urlConnection = null;
        try {
            urlConnection = (HttpURLConnection) new URL(urlString).openConnection();
            String apikey = token;
            urlConnection.setRequestMethod(action);
            urlConnection.addRequestProperty("Authorization", "Bearer " + apikey);
            urlConnection.setRequestProperty("Accept", "application/json");
            if (urlConnection.getResponseCode() == 200) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuffer buffer = new StringBuffer();
                while (true) {
                    String readLine = reader.readLine();
                    String line = readLine;
                    if (readLine == null) {
                        break;
                    }
                    buffer.append(line);
                }
                String stringBuffer = buffer.toString();
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                return stringBuffer;
            }
            Log.e("Error", "Error response code: " + urlConnection.getResponseCode());
            if (urlConnection == null) {
                return null;
            }
            urlConnection.disconnect();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            if (urlConnection == null) {
                return null;
            }
        } catch (Throwable th) {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            throw th;
        }
        return "";
    }

    public MapsFragment getMapFragment() {
        return this.mapFragment;
    }

    public Polyline getCurrentPolyline() {
        return this.currentPolyline;
    }

    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.change_data_nav /*2131230814*/:
                getSupportFragmentManager().beginTransaction().replace((int) R.id.fragment_container, (Fragment) new InitialNameFragment(), "InitialNameFragment").addToBackStack("InitialStepFragment").commit();
                break;
            case R.id.ddchistory /*2131230837*/:
                getSupportFragmentManager().beginTransaction().replace((int) R.id.fragment_container, (Fragment) new DatiDelleCorseFragment(), "DatiDelleCorseFragment").addToBackStack("PlaylistFragment").commit();
                break;
            case R.id.exit_nav /*2131230859*/:
                Intent startMain = new Intent("android.intent.action.MAIN");
                startMain.addCategory("android.intent.category.HOME");
                startMain.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(startMain);
                break;
            case R.id.home_nav /*2131230884*/:
                getSupportFragmentManager().beginTransaction().replace((int) R.id.fragment_container, (Fragment) this.mapFragment, "MapFragment").addToBackStack("MapFragment").commit();
                break;
            case R.id.playstore_nav /*2131231000*/:
                String appPackageName = getPackageName();
                try {
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + appPackageName)));
                    break;
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    break;
                }
            case R.id.select_playlist /*2131231037*/:
                getSupportFragmentManager().beginTransaction().replace((int) R.id.fragment_container, (Fragment) new PlaylistsFragment(this.playlists), "PlaylistFragment").addToBackStack("PlaylistFragment").commit();
                break;
            case R.id.slhistory /*2131231044*/:
                getSupportFragmentManager().beginTransaction().replace((int) R.id.fragment_container, (Fragment) new StepsLengthHistoryFragment(), "SLHFragment").addToBackStack("PlaylistFragment").commit();
                break;
        }
        this.drawerLayout.closeDrawer((int) GravityCompat.START);
        return true;
    }

    public void onTaskDone(Object... values) {
        this.mMap = MapsFragment.mapForActivity;
        Polyline polyline = this.currentPolyline;
        if (polyline != null) {
            polyline.remove();
        }
        PolylineOptions polylineOptions = (PolylineOptions) values[0];
        polylineOptions.width(15.0f).color(-16776961);
        this.currentPolyline = this.mMap.addPolyline(polylineOptions);
    }

    private class AsyncTaskShowUser extends AsyncTask<Void, Void, Void> {
        int cmStep;
        TextView headerTV;
        String nome;
        User uToShow;
        List<User> users;

        private AsyncTaskShowUser() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... voids) {
            List<User> user = MainActivity.mydb.mydao().getUser();
            this.users = user;
            User user2 = user.get(0);
            this.uToShow = user2;
            this.nome = user2.getName();
            this.cmStep = this.uToShow.getCmStep();
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.d("USER TO SHOW", "Username: " + this.uToShow.getName() + "Step length: " + this.uToShow.getCmStep());
            TextView textView = (TextView) ((NavigationView) MainActivity.this.findViewById(R.id.nav_view)).getHeaderView(0).findViewById(R.id.textViewHeader);
            this.headerTV = textView;
            textView.setText(MainActivity.this.getString(R.string.text_header, new Object[]{this.nome, Integer.valueOf(this.cmStep)}));
        }
    }

    private class SLHTask extends AsyncTask<Void, Void, Void> {
        private SLHTask() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... voids) {
            List unused = MainActivity.this.slhList = MainActivity.mydb.mydaoSH().getSLH();
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void aVoid) {
            int slhListLength = MainActivity.this.slhList.size();
            Log.d("slhsize", "" + slhListLength);
            MainActivity mainActivity = MainActivity.this;
            double sd = mainActivity.standardDeviation(mainActivity.slhList, slhListLength);
            if (sd < ((double) 5) || MainActivity.this.steptook) {
                int sum = 0;
                for (int i = 0; i < slhListLength; i++) {
                    sum += ((StepsLengthHistory) MainActivity.this.slhList.get(i)).getStepLength();
                }
                int i2 = sum / slhListLength;
                Log.d("variance&sd", "" + sd);
                double sd2 = Double.parseDouble(new DecimalFormat("#.##").format(sd).replace(",", "."));
                ((TextView) MainActivity.this.mapFragment.getView().findViewById(R.id.sdTW)).setText(MainActivity.this.getString(R.string.sd_value, new Object[]{String.valueOf(sd2)}));
            }
            super.onPostExecute(aVoid);
        }
    }

    private class ChangeStepLength extends AsyncTask<Integer, Void, Void> {
        User oldUser;
        List<User> users;

        private ChangeStepLength() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Integer... integers) {
            List<User> user = MainActivity.mydb.mydao().getUser();
            this.users = user;
            this.oldUser = user.get(0);
            MainActivity.mydb.mydao().removeOldUser();
            String nomeDaLasciare = this.oldUser.getName();
            int newCmStep = integers[0].intValue();
            User newUser = new User();
            newUser.setCmStep(newCmStep);
            newUser.setName(nomeDaLasciare);
            MainActivity.mydb.mydao().addUser(newUser);
            return null;
        }
    }

    private class JsonTask extends AsyncTask<String, String, String> {
        private JsonTask() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            return MainActivity.actionJSONFromURLConnection(params[0], "GET");
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            String unused = MainActivity.this.spotifyPlaylists = result;
            Log.d("SPOTIpLAYLISTS", MainActivity.this.spotifyPlaylists);
            try {
                JSONArray jArr = new JSONObject(MainActivity.this.spotifyPlaylists).getJSONArray("items");
                JSONArray unused2 = MainActivity.this.playlists = new JSONArray();
                for (int i = 0; i < jArr.length(); i++) {
                    JSONObject item = jArr.getJSONObject(i);
                    String name = item.getString("name");
                    String id = item.getString("id");
                    JSONArray access$700 = MainActivity.this.playlists;
                    access$700.put(new JSONObject("{\"name\":\"" + name + "\",\"id\":\"" + id + "\"}"));
                    StringBuilder sb = new StringBuilder();
                    sb.append(name);
                    sb.append("   ");
                    sb.append(jArr.length());
                    Log.d("PlaylistItem", sb.toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
