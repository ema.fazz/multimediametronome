package com.example.multimediametronome2;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.example.multimediametronome2.database.User;
import com.example.multimediametronome2.directionhelpers.FetchURL;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.material.navigation.NavigationView;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class MapsFragment extends Fragment implements OnMapReadyCallback {
    private static final float DEFAULT_ZOOM = 20.0f;
    private static final int LOCATION_REQUEST = 30;
    private static final String MY_API_KEY = "AIzaSyDGICldFs263W4ZmsiPAZc2rFwM7QJofo8";
    public static GoogleMap mapForActivity;
    /* access modifiers changed from: private */
    public Location currentLocation;
    private SharedPreferences.Editor editsp;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private ImageView imageViewGPS;
    private LocationCallback locationCallback;
    private LocationManager locationManager;
    private LocationRequest locationRequest;
    private Marker markerLocation;
    private MarkerOptions markerOptionLocation;
    /* access modifiers changed from: private */
    public MarkerOptions markerOptions;
    /* access modifiers changed from: private */
    public GoogleMap myMap;
    /* access modifiers changed from: private */
    public String snippetForClickMarker;

    /* renamed from: sp */
    private SharedPreferences f60sp;
    public ImageView startRoute;
    private View view;

    public View getView() {
        return this.view;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("prefs", 0);
        this.f60sp = sharedPreferences;
        this.editsp = sharedPreferences.edit();
        if (!Places.isInitialized()) {
            Places.initialize(getContext(), MY_API_KEY, Locale.ITALY);
        }
        LocationRequest create = LocationRequest.create();
        this.locationRequest = create;
        create.setPriority(100).setInterval(2000).setFastestInterval(1000).setMaxWaitTime(1000);
        getLocationCallback();
        this.fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getContext());
        this.locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(new StopServiceReceiver(), new IntentFilter("MainActivity"));
        Log.d("CREATEEE", "ONCREATEEE");
        if (!(ActivityCompat.checkSelfPermission(getActivity(), "android.permission.ACCESS_FINE_LOCATION") == 0 || ActivityCompat.checkSelfPermission(getActivity(), "android.permission.ACCESS_COARSE_LOCATION") == 0)) {
            startActivity(new Intent(getContext(), MainActivity.class));
        }
        this.fusedLocationProviderClient.requestLocationUpdates(this.locationRequest, this.locationCallback, Looper.getMainLooper());
    }

    public void onStart() {
        super.onStart();
        if (ActivityCompat.checkSelfPermission(getActivity(), "android.permission.ACCESS_FINE_LOCATION") == 0 && ActivityCompat.checkSelfPermission(getActivity(), "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            initMap();
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.fragment_maps, container, false);
        this.view = inflate;
        this.imageViewGPS = (ImageView) inflate.findViewById(R.id.gps_image);
        ImageView imageView = (ImageView) this.view.findViewById(R.id.start_route_image);
        this.startRoute = imageView;
        imageView.setImageResource(R.drawable.ic_directions_walk_black_24dp);
        this.startRoute.setVisibility(View.GONE);
        return this.view;
    }

    public void onViewCreated(View view2, Bundle savedInstanceState) {
        super.onViewCreated(view2, savedInstanceState);
    }

    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
    }

    public void onResume() {
        super.onResume();
        isLocationEnabled();
        if (this.currentLocation != null) {
            getDeviceLocation();
            moveCamera(new LatLng(this.currentLocation.getLatitude(), this.currentLocation.getLongitude()), 15.0f, "My Location");
        }
        new AsyncTaskShowUser().execute(new Void[0]);
        MainActivity mainActivity = (MainActivity) getActivity();
        if (Boolean.valueOf(this.f60sp.getBoolean("serviceIsStart", false)).booleanValue()) {
            this.startRoute.setVisibility(View.VISIBLE);
            setImageStopToStartRoute();
        } else if (mainActivity.getCurrentPolyline() != null) {
            this.startRoute.setVisibility(View.VISIBLE);
            setOnClickForPolyline(this.currentLocation, this.markerOptions.getPosition());
        }
    }

    public void onDestroy() {
        super.onDestroy();
        FusedLocationProviderClient fusedLocationProviderClient2 = this.fusedLocationProviderClient;
        if (fusedLocationProviderClient2 != null) {
            fusedLocationProviderClient2.removeLocationUpdates(this.locationCallback);
        }
    }

    public void onMapReady(GoogleMap googleMap) {
        if (ActivityCompat.checkSelfPermission(getActivity(), "android.permission.ACCESS_FINE_LOCATION") == 0 && ActivityCompat.checkSelfPermission(getActivity(), "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            this.myMap = googleMap;
            mapForActivity = googleMap;
            googleMap.setMyLocationEnabled(true);
            getDeviceLocation();
            initializeAutocompleteFragment();
            clickForMarker();
            gpsImageOnClick();
            this.myMap.getUiSettings().setMyLocationButtonEnabled(false);
        }
    }

    private void getLocationCallback() {
        this.locationCallback = new LocationCallback() {
            public void onLocationResult(LocationResult locationResult) {
                for (Location next : locationResult.getLocations()) {
                    MapsFragment.this.getDeviceLocation();
                    if (MapsFragment.this.markerOptions != null) {
                        LatLng markerPosition = MapsFragment.this.markerOptions.getPosition();
                        MapsFragment mapsFragment = MapsFragment.this;
                        mapsFragment.createPolyline(mapsFragment.currentLocation, markerPosition);
                    }
                }
            }
        };
    }

    public void setStartRouteVisibility(int visibility) {
        this.startRoute.setVisibility(visibility);
    }

    public void setImageStopToStartRoute() {
        this.startRoute.setImageResource(R.drawable.ic_man_stop);
        changeListenerStartRoute();
    }

    private void changeListenerStartRoute() {
        this.startRoute.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MapsFragment.this.getActivity().stopService(new Intent(MapsFragment.this.getActivity(), MetronomeService.class));
                MapsFragment.this.resetImageStartView();
            }
        });
    }

    public void setMarkerOptions(MarkerOptions markerOptions2) {
        this.markerOptions = markerOptions2;
    }

    public void resetImageStartView() {
        this.startRoute.setImageResource(R.drawable.ic_directions_walk_black_24dp);
        MarkerOptions markerOptions2 = this.markerOptions;
        if (markerOptions2 != null) {
            setOnClickForPolyline(this.currentLocation, markerOptions2.getPosition());
        }
    }

    private void initializeAutocompleteFragment() {
        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment) getChildFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        autocompleteFragment.setPlaceFields(Arrays.asList(new Place.Field[]{Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG}));
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            public void onPlaceSelected(Place place) {
                LatLng positionPlace = place.getLatLng();
                MapsFragment.this.startRoute.setVisibility(View.VISIBLE);
                if (MapsFragment.this.markerOptions != null) {
                    MapsFragment.this.myMap.clear();
                    MarkerOptions unused = MapsFragment.this.markerOptions = null;
                }
                MapsFragment.this.moveCamera(positionPlace, MapsFragment.DEFAULT_ZOOM, place.getName());
                MapsFragment mapsFragment = MapsFragment.this;
                mapsFragment.setOnClickForPolyline(mapsFragment.currentLocation, positionPlace);
                Log.i("PROVAPLACE", "Place: " + place.getName() + ", " + place.getId() + ", " + place.getLatLng());
            }

            public void onError(Status status) {
                Log.i("PROVAPLACE", "An error occurred: " + status);
            }
        });
    }

    private void clickForMarker() {
        this.myMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            public void onMapLongClick(LatLng latLng) {
                final LatLng latlngAddress = latLng;
                new Runnable() {
                    public void run() {
                        String unused = MapsFragment.this.snippetForClickMarker = MapsFragment.this.getAddress(MapsFragment.this.getContext(), latlngAddress.latitude, latlngAddress.longitude);
                    }
                }.run();
                MapsFragment.this.startRoute.setVisibility(View.VISIBLE);
                if (MapsFragment.this.markerOptions != null) {
                    MapsFragment.this.myMap.clear();
                    MarkerOptions unused = MapsFragment.this.markerOptions = null;
                }
                MarkerOptions unused2 = MapsFragment.this.markerOptions = new MarkerOptions().position(latLng).draggable(true).title("Destination address").snippet(MapsFragment.this.snippetForClickMarker);
                MapsFragment.this.myMap.addMarker(MapsFragment.this.markerOptions);
                MapsFragment mapsFragment = MapsFragment.this;
                mapsFragment.setOnClickForPolyline(mapsFragment.currentLocation, latLng);
            }
        });
    }

    /* access modifiers changed from: private */
    public void createPolyline(Location currentLocation2, LatLng destination) {
        new FetchURL(getContext()).execute(new String[]{getUrl(new LatLng(currentLocation2.getLatitude(), currentLocation2.getLongitude()), destination, "walking"), "walking"});
    }

    private void gpsImageOnClick() {
        this.imageViewGPS.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d("CLICK", "Clicked gps icon");
                MapsFragment.this.isLocationEnabled();
                if (MapsFragment.this.currentLocation != null) {
                    MapsFragment.this.moveCamera(new LatLng(MapsFragment.this.currentLocation.getLatitude(), MapsFragment.this.currentLocation.getLongitude()), MapsFragment.DEFAULT_ZOOM, "My Location");
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void setOnClickForPolyline(final Location startLocation, final LatLng endLocation) {
        this.startRoute.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MapsFragment.this.createPolyline(startLocation, endLocation);
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        MapsFragment.this.callMinuteFragment();
                    }
                }, 1000);
            }
        });
    }

    /* access modifiers changed from: private */
    public void callMinuteFragment() {
        getActivity().getSupportFragmentManager().beginTransaction().replace((int) R.id.frameMinuteFragment, (Fragment) new MinuteFragment(), "MinuteFragment").addToBackStack("MinuteFragment").commit();
    }

    private String getUrl(LatLng origin, LatLng dest, String directionMode) {
        return "https://maps.googleapis.com/maps/api/directions/" + "json" + "?" + (("origin=" + origin.latitude + "," + origin.longitude) + "&" + ("destination=" + dest.latitude + "," + dest.longitude) + "&" + ("mode=" + directionMode)) + "&key=" + getString(R.string.google_maps_key);
    }

    /* access modifiers changed from: private */
    public void getDeviceLocation() {
        try {
            this.fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener() {
                public void onComplete(Task task) {
                    if (task.isSuccessful()) {
                        Log.d("Location", "location found");
                        Location unused = MapsFragment.this.currentLocation = (Location) task.getResult();
                        return;
                    }
                    Log.d("Location", "location not found");
                }
            });
        } catch (SecurityException e) {
            Log.d("Exception", e.getMessage());
        }
    }

    /* access modifiers changed from: private */
    public void moveCamera(LatLng latLng, float zoom, String snippet) {
        if(myMap!=null) {
            this.myMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
            if (!snippet.equals("My Location")) {
                MarkerOptions snippet2 = new MarkerOptions().position(latLng).draggable(true).title("Destination address").snippet(snippet);
                this.markerOptions = snippet2;
                this.myMap.addMarker(snippet2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void isLocationEnabled() {
        if (!this.locationManager.isProviderEnabled("gps")) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("Abilita il GPS");
            alertDialog.setMessage("Il tuo GPS è disabilitato. Abilitalo nelle impostazioni. Senza GPS non puoi usare l'app.");
            alertDialog.setPositiveButton("Impostazioni GPS", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    MapsFragment.this.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
                    dialog.cancel();
                }
            });
            alertDialog.setNegativeButton("Annulla", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    MapsFragment.this.getActivity().finishAffinity();
                }
            });
            AlertDialog alert = alertDialog.create();
            alert.setCanceledOnTouchOutside(false);
            alert.show();
        }
    }

    public String getAddress(Context context, double lat, double lng) {
        try {
            Address markerAddress = new Geocoder(context, Locale.getDefault()).getFromLocation(lat, lng, 1).get(0);
            return ((((((markerAddress.getAddressLine(0) + "\n" + markerAddress.getCountryName()) + "\n" + markerAddress.getCountryCode()) + "\n" + markerAddress.getAdminArea()) + "\n" + markerAddress.getPostalCode()) + "\n" + markerAddress.getSubAdminArea()) + "\n" + markerAddress.getLocality()) + "\n" + markerAddress.getSubThoroughfare();
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("IOEXCEPTION", e.getMessage());
            return null;
        }
    }

    private class AsyncTaskShowUser extends AsyncTask<Void, Void, Void> {
        int cmStep;
        TextView headerTV;
        String nome;
        User uToShow;
        List<User> users;

        private AsyncTaskShowUser() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... voids) {
            List<User> user = MainActivity.mydb.mydao().getUser();
            this.users = user;
            User user2 = user.get(0);
            this.uToShow = user2;
            this.nome = user2.getName();
            this.cmStep = this.uToShow.getCmStep();
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.d("USER TO SHOW", "Username: " + this.uToShow.getName() + "Step length: " + this.uToShow.getCmStep());
            TextView textView = (TextView) ((NavigationView) MapsFragment.this.getActivity().findViewById(R.id.nav_view)).getHeaderView(0).findViewById(R.id.textViewHeader);
            this.headerTV = textView;
            textView.setText(MapsFragment.this.getString(R.string.text_header, this.nome, Integer.valueOf(this.cmStep)));
        }
    }
}
