package com.example.multimediametronome2.initialsetup;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.multimediametronome2.R;

public class InitialNameFragment extends Fragment implements View.OnClickListener {
    private String nome = "";
    private EditText nomeEditText;
    private Bundle sis;
    private View view;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.fragment_initial_name, container, false);
        this.view = inflate;
        this.sis = savedInstanceState;
        this.nomeEditText = (EditText) this.view.findViewById(R.id.editTextName);
        ((Button) inflate.findViewById(R.id.button_avanti_name)).setOnClickListener(this);
        return this.view;
    }

    public void onClick(View v) {
        FragmentTransaction trans = getActivity().getSupportFragmentManager().beginTransaction();
        if (v.findViewById(R.id.fragment_container) == null || this.sis == null) {
            String obj = this.nomeEditText.getText().toString();
            this.nome = obj;
            if (obj.equals("")) {
                Toast.makeText(getContext(),"Inserisci per favore n nome",1000).show();
                return;
            }
            InitialStepFragment stepFragment = new InitialStepFragment();
            stepFragment.setNome(this.nome);
            trans.replace((int) R.id.fragment_container, (Fragment) stepFragment, "StepFragment");
            trans.addToBackStack("StepFragment");
            trans.commit();
        }
    }
}
