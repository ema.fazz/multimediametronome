package com.example.multimediametronome2.initialsetup;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.multimediametronome2.R;

public class InfoStepFragment extends Fragment {
    String nome;

    public void setNome(String nome2) {
        this.nome = nome2;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info_step, container, false);
        final Bundle sis = savedInstanceState;
        ((Button) view.findViewById(R.id.buttonIndietroInfo)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FragmentTransaction trans = InfoStepFragment.this.getActivity().getSupportFragmentManager().beginTransaction();
                if (v.findViewById(R.id.fragment_container) == null || sis == null) {
                    InitialStepFragment initialStepFragment = new InitialStepFragment();
                    initialStepFragment.setNome(InfoStepFragment.this.nome);
                    trans.replace((int) R.id.fragment_container, (Fragment) initialStepFragment, "InfoStepFragment");
                    trans.addToBackStack((String) null);
                    trans.commit();
                }
            }
        });
        return view;
    }
}
