package com.example.multimediametronome2.initialsetup;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.multimediametronome2.R;

public class InitialSetupFragment extends Fragment {
    Button next;
    Bundle sis;
    View view;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.fragment_initial_setup, container, false);
        this.view = inflate;
        Button button = (Button) inflate.findViewById(R.id.button_inizia_su);
        this.next = button;
        this.sis = savedInstanceState;
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                InitialSetupFragment initialSetupFragment = InitialSetupFragment.this;
                initialSetupFragment.callnFragment(initialSetupFragment.sis);
            }
        });
        return this.view;
    }

    /* access modifiers changed from: private */
    public void callnFragment(Bundle savedInstanceState) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        if (this.view.findViewById(R.id.fragment_container) == null || savedInstanceState == null) {
            FragmentTransaction transaction = fm.beginTransaction();
            transaction.replace(R.id.fragment_container, new InitialNameFragment());
            transaction.addToBackStack((String) null);
            transaction.commit();
        }
    }
}
