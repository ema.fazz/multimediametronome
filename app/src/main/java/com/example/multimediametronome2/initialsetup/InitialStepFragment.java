package com.example.multimediametronome2.initialsetup;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.example.multimediametronome2.InitialActivity;
import com.example.multimediametronome2.MainActivity;
import com.example.multimediametronome2.R;
import com.example.multimediametronome2.database.User;

public class InitialStepFragment extends Fragment {
    private static final double DELTA_F = 0.413d;
    private static final double DELTA_M = 0.415d;
    /* access modifiers changed from: private */
    public int cmStep;
    /* access modifiers changed from: private */
    public String nome;
    /* access modifiers changed from: private */
    public NumberPicker npHeight;
    private RadioButton rbFemale;
    private RadioButton rbMale;
    /* access modifiers changed from: private */
    public RadioGroup rgFeMale;
    /* access modifiers changed from: private */
    public Bundle sis;
    private View view;

    public void setNome(String nome2) {
        this.nome = nome2;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.fragment_initial_step, container, false);
        this.view = inflate;
        this.sis = savedInstanceState;
        this.npHeight = (NumberPicker) inflate.findViewById(R.id.heightPicker);
        this.rgFeMale = (RadioGroup) this.view.findViewById(R.id.rgFeMale);
        this.rbFemale = (RadioButton) this.view.findViewById(R.id.rbFemale);
        this.npHeight.setMinValue(0);
        this.npHeight.setMaxValue(300);
        this.npHeight.setValue(165);
        this.rbFemale.setChecked(true);
        ((TextView) this.view.findViewById(R.id.textViewStep)).setText("Piacere di conoscerti " + this.nome + ",\ninserisci il tuo sesso e la tua altezza per determinare la tua lunghezza del passo");
        ((ImageButton) this.view.findViewById(R.id.imageButtonInfo)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FragmentTransaction trans = InitialStepFragment.this.getActivity().getSupportFragmentManager().beginTransaction();
                if (v.findViewById(R.id.fragment_container) == null || InitialStepFragment.this.sis == null) {
                    InfoStepFragment infoFragment = new InfoStepFragment();
                    infoFragment.setNome(InitialStepFragment.this.nome);
                    trans.replace(R.id.fragment_container, infoFragment);
                    trans.addToBackStack("FragmentStepInfo");
                    trans.commit();
                }
            }
        });
        ((Button) this.view.findViewById(R.id.button_avanti_step)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int height = InitialStepFragment.this.npHeight.getValue();
                if (InitialStepFragment.this.rgFeMale.getCheckedRadioButtonId() == R.id.rbFemale) {
                    int unused = InitialStepFragment.this.cmStep = (int) (((double) height) * InitialStepFragment.DELTA_F);
                } else {
                    int unused2 = InitialStepFragment.this.cmStep = (int) (((double) height) * InitialStepFragment.DELTA_M);
                }
                User u = new User();
                u.setName(InitialStepFragment.this.nome);
                u.setCmStep(InitialStepFragment.this.cmStep);
                new AsyncTaskSaveUser().execute(new User[]{u});
                FragmentActivity activity = InitialStepFragment.this.getActivity();
                InitialStepFragment.this.getActivity();
                SharedPreferences.Editor edit = activity.getSharedPreferences("prefs", 0).edit();
                edit.putBoolean("init", false);
                edit.apply();
            }
        });
        return this.view;
    }

    private class AsyncTaskSaveUser extends AsyncTask<User, Void, Void> {
        private AsyncTaskSaveUser() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(User... users) {
            MainActivity.mydb.mydao().removeOldUser();
            MainActivity.mydb.mydao().addUser(users[0]);
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void aVoid) {
            if (InitialStepFragment.this.getActivity() instanceof InitialActivity) {
                InitialStepFragment.this.startActivity(new Intent(InitialStepFragment.this.getActivity(), MainActivity.class));
                InitialStepFragment.this.getActivity().finish();
            } else {
                InitialStepFragment.this.getActivity().getSupportFragmentManager().beginTransaction().replace((int) R.id.fragment_container, (Fragment) ((MainActivity) InitialStepFragment.this.getActivity()).getMapFragment(), "MapFragment").addToBackStack("MapFragment").commit();
            }
            Toast.makeText(InitialStepFragment.this.getActivity(), "User added", 0).show();
        }
    }
}
