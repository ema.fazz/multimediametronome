package com.example.multimediametronome2;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;
import androidx.core.app.NotificationCompat;
import com.example.multimediametronome2.database.StepsLengthHistory;
import com.example.multimediametronome2.database.User;
import com.spotify.android.appremote.api.ConnectionParams;
import com.spotify.android.appremote.api.Connector;
import com.spotify.android.appremote.api.PlayerApi;
import com.spotify.android.appremote.api.SpotifyAppRemote;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MetronomeService extends Service implements SensorEventListener {
    private static final String CHANNEL_ID = "NotificationServiceChannel";
    private static final String CLIENT_ID = "383a3374409d4589bb7e03658c38ef15";
    private static final int MINBPM = 80;
    private static final String REDIRECT_URI = "multimedia-metronome://callback";
    /* access modifiers changed from: private */

    /* renamed from: sp */
    public static SharedPreferences f61sp;
    /* access modifiers changed from: private */
    public String ENDPOINTPLAYLISTTRACKS;
    /* access modifiers changed from: private */
    public String ENDPOINTTRACKSFEATURES;
    /* access modifiers changed from: private */
    public MediaPlayer bip;
    /* access modifiers changed from: private */
    public double bpm;
    private boolean checkDeltaOnlyOnce = true;
    /* access modifiers changed from: private */
    public int cmStep;
    /* access modifiers changed from: private */
    public int countTrackInQueue;
    private String dataDellaCorsa;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    /* access modifiers changed from: private */
    public DecimalFormat decimalFormat;
    private ArrayList<Integer> deltaVisited = new ArrayList<>();
    private ArrayList<Integer> deltaVisitedInWindow = new ArrayList<>();
    /* access modifiers changed from: private */
    public float distanceRemain;
    private boolean eventFirst;
    /* access modifiers changed from: private */
    public boolean flagbip;
    private Timer getBPMTimer;
    private TimerTask getBPMTimerTask;
    private float initialDistance;
    private int initialStep = 0;
    /* access modifiers changed from: private */
    public boolean isSpotifyInstalled;

    /* renamed from: k */
    private int f62k = 0;
    private int lengthStepsArray = 0;
    /* access modifiers changed from: private */
    public SpotifyAppRemote mSpotifyAppRemote;
    private Handler mainHandler;
    /* access modifiers changed from: private */
    public Service metronomeService;
    private String playlistID;
    /* access modifiers changed from: private */
    public int remainSeconds;
    private boolean running = false;
    private int seconds;
    private SensorManager sensorManager;
    /* access modifiers changed from: private */
    public Timer setMetronomeTimer;
    private TimerTask setMetronomeTimerTask;
    /* access modifiers changed from: private */
    public ArrayList<Double> songTempo;
    private ArrayList<Integer> stepsArray;
    private ArrayList<Integer> stepsInDelta = new ArrayList<>();
    /* access modifiers changed from: private */
    public double timeForBip;
    private int totalStep = 0;
    /* access modifiers changed from: private */
    public ArrayList<Integer> trackDuration;
    /* access modifiers changed from: private */
    public ArrayList<String> tracksToPlay;
    private String valuesInStepsArray = "";

    static /* synthetic */ int access$1508(MetronomeService x0) {
        int i = x0.countTrackInQueue;
        x0.countTrackInQueue = i + 1;
        return i;
    }

    public void onCreate() {
        this.sensorManager = (SensorManager) getSystemService("sensor");
        this.mainHandler = new Handler(getMainLooper());
        super.onCreate();
        new AsyncTaskShowUser().execute(new Void[0]);
        this.metronomeService = this;
        this.flagbip = false;
        f61sp = getSharedPreferences("prefs", 0);
        this.decimalFormat = new DecimalFormat("##.##");
        float distRemain = f61sp.getFloat("distancePolyline", 0.0f);
        this.distanceRemain = Float.parseFloat(this.decimalFormat.format((double) distRemain).replace(",", "."));
        this.initialDistance = distRemain;
        this.stepsArray = new ArrayList<>();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        this.dataDellaCorsa = this.dateFormat.format(new Date());
        int intExtra = intent.getIntExtra("seconds", 0);
        this.seconds = intExtra;
        this.initialStep = 0;
        this.totalStep = 0;
        this.countTrackInQueue = 0;
        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setAction("android.intent.action.MAIN");
        notificationIntent.addCategory("android.intent.category.LAUNCHER");
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        PendingIntent pStopService = PendingIntent.getBroadcast(this, 0, new Intent(getApplicationContext(), StopServiceReceiver.class), 268435456);
        NotificationCompat.Builder contentTitle = new NotificationCompat.Builder(this, CHANNEL_ID).setContentTitle("Il Metronomo è partito.");
        startForeground(1, contentTitle.setContentText("Tempo totale: " + String.valueOf(intExtra / 60) + " minuti - Distanza totale: " + this.distanceRemain + " metri").setSmallIcon(R.drawable.ic_timer_black_24dp).setContentIntent(pendingIntent).addAction(R.drawable.ic_timer_black_24dp, "STOP", pStopService).build());
        try {
            getPackageManager().getPackageInfo("com.spotify.music", 0);
            this.isSpotifyInstalled = true;
        } catch (PackageManager.NameNotFoundException e) {
            this.isSpotifyInstalled = false;
        }
        startMetronome();
        return 1;
    }

    public void onDestroy() {
        String writeOnFile;
        this.getBPMTimer.cancel();
        this.running = false;
        this.sensorManager.unregisterListener(this);
        SharedPreferences.Editor editsp = f61sp.edit();
        editsp.putBoolean("serviceIsStart", false);
        editsp.apply();
        if (this.mSpotifyAppRemote != null && this.setMetronomeTimer == null) {
            for (int i = 0; i < this.countTrackInQueue; i++) {
                Log.d("countTrackInQueue", "" + this.countTrackInQueue);
                this.mSpotifyAppRemote.getPlayerApi().skipNext();
            }
        } else if (this.mSpotifyAppRemote != null && this.setMetronomeTimer != null) {
            for (int i2 = 0; i2 < this.countTrackInQueue; i2++) {
                Log.d("countTrackInQueue", "" + this.countTrackInQueue + " i: " + i2);
                this.mSpotifyAppRemote.getPlayerApi().skipNext();
            }
            this.bip.release();
            this.setMetronomeTimer.cancel();
        } else if (this.mSpotifyAppRemote == null && this.setMetronomeTimer != null) {
            this.bip.release();
            this.setMetronomeTimer.cancel();
        }
        Toast.makeText(this, "Il metronomo si è fermato.", 0).show();
        ArrayList<Integer> sANoOutliers = new ArrayList<>();
        if (this.valuesInStepsArray != "") {
            int sum = 0;
            for (int i3 = 0; i3 < this.stepsArray.size(); i3++) {
                sum += this.stepsArray.get(i3).intValue();
            }
            int stepAVG = sum / this.stepsArray.size();
            for (int i4 = 0; i4 < this.stepsArray.size(); i4++) {
                if (this.stepsArray.get(i4).intValue() <= stepAVG + 50 || this.stepsArray.get(i4).intValue() >= stepAVG - 50) {
                    sANoOutliers.add(this.stepsArray.get(i4));
                }
            }
            double sd = standardDeviation(sANoOutliers, sANoOutliers.size());
            if (sANoOutliers.size() > 0) {
                writeOnFile = "Corsa del: " + this.dataDellaCorsa + "\nMedia di stepsArray: " + stepAVG + "\nArray dei valori SW con outliesrs: " + this.valuesInStepsArray + "\nSD no outliers: " + sd + "\n\n";
            } else {
                writeOnFile = "Corsa del: " + this.dataDellaCorsa + "\nI calcoli hanno fallito. Nessun valore riscontrato";
            }
            writeToFile(writeOnFile, getApplicationContext());
            String stepsInDeltaString = "";
            for (int i5 = 0; i5 < this.stepsInDelta.size(); i5++) {
                stepsInDeltaString = stepsInDeltaString + this.stepsInDelta.get(i5).toString() + " - ";
            }
            writeToFile(stepsInDeltaString, getApplicationContext());
            if (sANoOutliers.size() > 0) {
                new InsertStepLengthTask(stepAVG, (int) sd).execute(new Void[0]);
            }
        }
        super.onDestroy();
        if (this.mSpotifyAppRemote != null) {
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    MetronomeService.this.mSpotifyAppRemote.getPlayerApi().pause();
                    Log.d("ISDISCONNECTSPOTY", "disconnetto spoty");
                    SpotifyAppRemote.disconnect(MetronomeService.this.mSpotifyAppRemote);
                }
            }, 3000);
        }
    }

    private void writeToFile(String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("datiCorse2.txt", 32768));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    /* access modifiers changed from: private */
    public void makeToast(final String s) {
        this.mainHandler.post(new Runnable() {
            public void run() {
                Toast.makeText(MetronomeService.this.getApplicationContext(), s, 1).show();
            }
        });
    }

    static double variance(ArrayList<Integer> a, int n) {
        double sum = 0.0d;
        for (int i = 0; i < n; i++) {
            sum += (double) a.get(i).intValue();
        }
        double mean = sum / ((double) n);
        double sqDiff = 0.0d;
        for (int i2 = 0; i2 < n; i2++) {
            sqDiff += (((double) a.get(i2).intValue()) - mean) * (((double) a.get(i2).intValue()) - mean);
        }
        return sqDiff / ((double) n);
    }

    static double standardDeviation(ArrayList<Integer> a, int n) {
        return Math.sqrt(variance(a, n));
    }

    private void startMetronome() {
        this.eventFirst = true;
        Sensor counterSensor = this.sensorManager.getDefaultSensor(19);
        if (counterSensor != null) {
            makeToast("registro il sensore.");
            this.sensorManager.registerListener(this, counterSensor, 0);
            this.running = true;
        } else {
            makeToast("Sensore non trovato!");
        }
        SharedPreferences.Editor editsp = f61sp.edit();
        editsp.putBoolean("serviceIsStart", true);
        editsp.apply();
        this.playlistID = f61sp.getString("plIDSelected", "");
        String str = "https://api.spotify.com/v1/playlists/" + this.playlistID + "/tracks";
        this.ENDPOINTPLAYLISTTRACKS = str;
        Log.d("ENDPOINTPLAYLISTTRACKS", str);
        this.remainSeconds = this.seconds;
        this.getBPMTimer = new Timer();
        this.getBPMTimerTask = new TimerTask() {
            public void run() {
                ArrayList unused = MetronomeService.this.tracksToPlay = new ArrayList();
                ArrayList unused2 = MetronomeService.this.songTempo = new ArrayList();
                ArrayList unused3 = MetronomeService.this.trackDuration = new ArrayList();
                if (MetronomeService.this.remainSeconds <= 0) {
                    MetronomeService.this.metronomeService.stopSelf();
                    Log.d("TIMEFINISH", String.valueOf(MetronomeService.this.remainSeconds));
                    return;
                }
                float distRemain = MetronomeService.f61sp.getFloat("distancePolyline", 0.0f);
                MetronomeService metronomeService = MetronomeService.this;
                float unused4 = metronomeService.distanceRemain = Float.parseFloat(metronomeService.decimalFormat.format((double) distRemain).replace(",", "."));
                int remainStep = (((int) MetronomeService.this.distanceRemain) * 100) / MetronomeService.this.cmStep;
                Log.d("remainStep", String.valueOf(remainStep));
                double bps = ((double) remainStep) / ((double) MetronomeService.this.remainSeconds);
                double unused5 = MetronomeService.this.bpm = 60.0d * bps;
                Log.d("BPS", String.valueOf(bps));
                double unused6 = MetronomeService.this.timeForBip = 1000.0d / bps;
                if (MetronomeService.this.timeForBip < 150.0d) {
                    MetronomeService.this.makeToast("BPM troppo alti. Fare un altro percorso.");
                    MetronomeService.this.onDestroy();
                }
                if (bps < 1.0d) {
                    double unused7 = MetronomeService.this.timeForBip = (1.0d / bps) * 1000.0d;
                }
                Log.d("TIMEFORBIP", String.valueOf(MetronomeService.this.timeForBip));
                if (MetronomeService.this.isSpotifyInstalled) {
                    if (MetronomeService.this.countTrackInQueue > 0) {
                        for (int i = 0; i < MetronomeService.this.countTrackInQueue; i++) {
                            Log.d("countTrackInQueue", "" + MetronomeService.this.countTrackInQueue);
                            MetronomeService.this.mSpotifyAppRemote.getPlayerApi().skipNext();
                        }
                        int unused8 = MetronomeService.this.countTrackInQueue = 0;
                    }
                    new JsonTask().execute(new String[]{MetronomeService.this.ENDPOINTPLAYLISTTRACKS});
                } else {
                    MetronomeService metronomeService2 = MetronomeService.this;
                    metronomeService2.metronomeBeep(metronomeService2.timeForBip);
                    MetronomeService.this.makeToast("Per usare spotify come metronomo bisogna averlo installato.");
                }
                if (MetronomeService.this.flagbip && MetronomeService.this.setMetronomeTimer != null) {
                    MetronomeService.this.setMetronomeTimer.cancel();
                }
                MetronomeService metronomeService3 = MetronomeService.this;
                int unused9 = metronomeService3.remainSeconds = metronomeService3.remainSeconds - 120;
                boolean unused10 = MetronomeService.this.flagbip = true;
            }
        };
        this.getBPMTimer.scheduleAtFixedRate(getBPMTimerTask, 1000, 120000);
        Log.d("BPM", "" + f61sp.getFloat("BPM", 0.0f));
    }

    /* access modifiers changed from: private */
    public double findMinBPM(ArrayList<Double> a) {
        double min = a.get(0).doubleValue();
        for (int i = 1; i < a.size(); i++) {
            if (a.get(i).doubleValue() < min) {
                min = a.get(i).doubleValue();
            }
        }
        return min;
    }

    /* access modifiers changed from: private */
    public double findMaxBPM(ArrayList<Double> a) {
        double max = a.get(0).doubleValue();
        for (int i = 1; i < a.size(); i++) {
            if (a.get(i).doubleValue() > max) {
                max = a.get(i).doubleValue();
            }
        }
        return max;
    }

    /* access modifiers changed from: private */
    public void connectSpotify(final String firstTrack) {
        ConnectionParams connectionParams = new ConnectionParams.Builder(CLIENT_ID).setRedirectUri(REDIRECT_URI).showAuthView(true).build();
        Log.d("ENTRATOCONNSPOTIFY", "ENTRATOOOOOO");
        SpotifyAppRemote.connect(this, connectionParams, new Connector.ConnectionListener() {
            public void onConnected(SpotifyAppRemote spotifyAppRemote) {
                SpotifyAppRemote unused = MetronomeService.this.mSpotifyAppRemote = spotifyAppRemote;
                Log.d("MainActivity", "Connected! Yay!");
                PlayerApi playerApi = MetronomeService.this.mSpotifyAppRemote.getPlayerApi();
                playerApi.play("spotify:track:" + firstTrack);
            }

            public void onFailure(Throwable throwable) {
                Log.e("MyActivity", throwable.getMessage(), throwable);
            }
        });
    }

    /* access modifiers changed from: private */
    public void metronomeBeep(double timeForBip2) {
        SpotifyAppRemote spotifyAppRemote = this.mSpotifyAppRemote;
        if (spotifyAppRemote != null) {
            spotifyAppRemote.getPlayerApi().pause();
        }
        this.setMetronomeTimer = new Timer();
        setMetronomeTimerTask = new TimerTask() {
            public void run() {
                MetronomeService metronomeService = MetronomeService.this;
                MediaPlayer unused = metronomeService.bip = MediaPlayer.create(metronomeService.getApplicationContext(), R.raw.bip);
                MetronomeService.this.bip.start();
                MetronomeService.this.bip.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                    }
                });
            }
        };
        this.setMetronomeTimer.scheduleAtFixedRate(setMetronomeTimerTask, 0, (long) timeForBip2);
    }

    /* access modifiers changed from: private */
    public double findNearHighBPM(JSONArray jsona, double bpmnow) throws JSONException {
        ArrayList<Double> tempos = new ArrayList<>();
        int i = 0;
        while (i < jsona.length() && i != jsona.length() - 1) {
            JSONObject track = null;
            if (jsona.getJSONObject(i) != null) {
                track = jsona.getJSONObject(i);
            }
            double tempo = track.getDouble("tempo");
            if (tempo > bpmnow) {
                tempos.add(Double.valueOf(tempo));
            }
            i++;
        }
        return findMinBPM(tempos);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    private double round(double num, int multipleOf) {
        return Math.floor(((((double) multipleOf) / 2.0d) + num) / ((double) multipleOf)) * ((double) multipleOf);
    }

    public void onSensorChanged(SensorEvent event) {
        SensorEvent sensorEvent = event;
        if (this.running) {
            float distRemain = f61sp.getFloat("distancePolyline", 0.0f);
            Log.d("ENTRATOONSENSORCHANGE", "ENTRATOOOOOOOOOOO");
            if (distRemain > this.initialDistance + 20.0f) {
                makeToast("Segui la polyline se vuoi che l'app misuri i tuoi passi.");
                this.eventFirst = true;
                return;
            }
            if (this.eventFirst) {
                Log.d("initialStepif", "" + this.initialStep);
                this.initialStep = (int) sensorEvent.values[0];
                this.eventFirst = false;
            }
            Log.d("initialStep", "" + this.initialStep);
            Log.d("difstepsensor", "totalStep: " + this.totalStep + " initialStep: " + this.initialStep + " event values: " + sensorEvent.values[0]);
            this.totalStep = (int) (sensorEvent.values[0] - ((float) this.initialStep));
            Log.d("difstepsensor", "totalStep: " + this.totalStep + " initialStep: " + this.initialStep + " event values: " + sensorEvent.values[0]);
            float differenceDistance = this.initialDistance - distRemain;
            boolean enterInDelta = differenceDistance % ((float) 20) < 3.0f || differenceDistance % ((float) 20) > ((float) (20 + -3));
            if (differenceDistance > 5.0f && enterInDelta) {
                int roundOfDiffDist = (int) round((double) differenceDistance, 20);
                int moduleOfRoundAndDelta = roundOfDiffDist / 20;
                makeToast("in delta: moduleOfRoundAndDelta: " + moduleOfRoundAndDelta + " roundOfDiffDist:" + roundOfDiffDist);
                if (this.deltaVisited.contains(Integer.valueOf(moduleOfRoundAndDelta))) {
                    this.checkDeltaOnlyOnce = false;
                } else {
                    this.checkDeltaOnlyOnce = true;
                    this.deltaVisited.add(Integer.valueOf(moduleOfRoundAndDelta));
                }
                if (this.checkDeltaOnlyOnce) {
                    if (this.stepsInDelta.size() > 0) {
                        int totStepsDelta = 0;
                        for (int i = 0; i < this.stepsInDelta.size(); i++) {
                            totStepsDelta += this.stepsInDelta.get(i).intValue();
                        }
                        int i2 = this.totalStep;
                        if (i2 - totStepsDelta < 10) {
                            ArrayList<Integer> arrayList = this.stepsInDelta;
                            int stepsInDeltaLastPosition = arrayList.get(arrayList.size() - 1).intValue();
                            ArrayList<Integer> arrayList2 = this.stepsInDelta;
                            arrayList2.set(arrayList2.size() - 1, Integer.valueOf((i2 - totStepsDelta) + stepsInDeltaLastPosition));
                        } else {
                            this.stepsInDelta.add(Integer.valueOf(i2 - totStepsDelta));
                        }
                        StringBuilder sb = new StringBuilder();
                        sb.append("stepsInDelta-1 con for: ");
                        ArrayList<Integer> arrayList3 = this.stepsInDelta;
                        sb.append(arrayList3.get(arrayList3.size() - 1));
                        makeToast(sb.toString());
                    } else {
                        this.stepsInDelta.add(Integer.valueOf(this.totalStep));
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("stepsInDelta-1: ");
                        ArrayList<Integer> arrayList4 = this.stepsInDelta;
                        sb2.append(arrayList4.get(arrayList4.size() - 1));
                        makeToast(sb2.toString());
                    }
                }
                this.deltaVisited.add(Integer.valueOf(moduleOfRoundAndDelta));
            }
            if (((double) differenceDistance) >= 100.0d && enterInDelta) {
                int roundOfDiffDist2 = (int) round((double) differenceDistance, 20);
                int moduleOfRoundAndDelta2 = roundOfDiffDist2 / 20;
                makeToast("in window: moduleOfRoundAndDelta: " + moduleOfRoundAndDelta2 + " roundOfDiffDist:" + roundOfDiffDist2);
                if (this.deltaVisitedInWindow.contains(Integer.valueOf(moduleOfRoundAndDelta2))) {
                    this.checkDeltaOnlyOnce = false;
                } else {
                    this.checkDeltaOnlyOnce = true;
                    this.deltaVisitedInWindow.add(Integer.valueOf(moduleOfRoundAndDelta2));
                }
                if (this.checkDeltaOnlyOnce) {
                    double totStepSlidingWindow = 0.0d;
                    for (int i3 = this.f62k; i3 < this.stepsInDelta.size(); i3++) {
                        totStepSlidingWindow += (double) this.stepsInDelta.get(i3).intValue();
                    }
                    double sldouble = 100.0d / totStepSlidingWindow;
                    makeToast("sldouble: " + sldouble);
                    int stepLength = (int) (100.0d * sldouble);
                    makeToast("stepLength: " + stepLength);
                    this.stepsArray.add(Integer.valueOf(stepLength));
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("stepsArray: ");
                    int i4 = roundOfDiffDist2;
                    sb3.append(this.stepsArray.get(this.lengthStepsArray));
                    sb3.append(" totStepSlidingWindow: ");
                    sb3.append(totStepSlidingWindow);
                    makeToast(sb3.toString());
                    this.valuesInStepsArray += this.stepsArray.get(this.lengthStepsArray) + "_" + totStepSlidingWindow + " - ";
                    makeToast("stepsArray size:" + this.lengthStepsArray);
                    this.f62k = this.f62k + 1;
                    this.lengthStepsArray = this.lengthStepsArray + 1;
                    return;
                }
            }
        }
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    private class InsertStepLengthTask extends AsyncTask<Void, Void, Void> {
        String currentDate = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault()).format(new Date());

        /* renamed from: sd */
        private int f63sd;
        StepsLengthHistory slh = new StepsLengthHistory();
        private int stepLength;

        public InsertStepLengthTask(int stepLength2, int sd) {
            this.stepLength = stepLength2;
            this.f63sd = sd;
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... voids) {
            this.slh.setStepLength(this.stepLength);
            this.slh.setRegistrationDate(this.currentDate);
            this.slh.setStandardDeviation(this.f63sd);
            MainActivity.mydb.mydaoSH().addStep(this.slh);
            return null;
        }
    }

    private class AsyncTaskShowUser extends AsyncTask<Void, Void, Void> {
        User uToShow;
        List<User> users;

        private AsyncTaskShowUser() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... voids) {
            List<User> user = MainActivity.mydb.mydao().getUser();
            this.users = user;
            this.uToShow = user.get(0);
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            int unused = MetronomeService.this.cmStep = this.uToShow.getCmStep();
        }
    }

    private class JsonTask extends AsyncTask<String, String, String> {
        private JsonTask() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            return MainActivity.actionJSONFromURLConnection(params[0], "GET");
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String s) {
            Log.d("STRINGS", "string: " + s);
            if (s != null) {
                try {
                    JSONArray tracks = new JSONObject(s).getJSONArray("items");
                    String unused = MetronomeService.this.ENDPOINTTRACKSFEATURES = "https://api.spotify.com/v1/audio-features?ids=";
                    for (int i = 0; i < tracks.length(); i++) {
                        JSONObject track = tracks.getJSONObject(i).getJSONObject("track");
                        String trackID = "";
                        if (track.getString("id") != null) {
                            trackID = track.getString("id");
                        }
                        if (i == tracks.length() - 1) {
                            MetronomeService metronomeService = MetronomeService.this;
                            String unused2 = metronomeService.ENDPOINTTRACKSFEATURES = MetronomeService.this.ENDPOINTTRACKSFEATURES + trackID;
                        } else {
                            MetronomeService metronomeService2 = MetronomeService.this;
                            String unused3 = metronomeService2.ENDPOINTTRACKSFEATURES = MetronomeService.this.ENDPOINTTRACKSFEATURES + trackID + "%2C";
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d("ENDPOINTTRACKSFEATURES", MetronomeService.this.ENDPOINTTRACKSFEATURES);
                new JsonTaskBPM().execute(new String[]{MetronomeService.this.ENDPOINTTRACKSFEATURES});
            }
        }
    }

    private class JsonTaskBPM extends AsyncTask<String, String, String> {
        boolean canSpotifyPlay;

        private JsonTaskBPM() {
            this.canSpotifyPlay = true;
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            return MainActivity.actionJSONFromURLConnection(params[0], "GET");
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String s) {
            String str;
            String str2;
            String str3 = s;
            String str4 = "";
            super.onPostExecute(s);
            Log.d("SSSSSSSSSSS", str3);
            try {
                JSONObject result = new JSONObject(str3);
                JSONArray audioFeatures = result.getJSONArray("audio_features");
                int i = 0;
                while (true) {
                    if (i >= audioFeatures.length()) {
                        break;
                    } else if (i == audioFeatures.length() - 1) {
                        break;
                    } else {
                        JSONObject track = null;
                        if (audioFeatures.getJSONObject(i) != null) {
                            track = audioFeatures.getJSONObject(i);
                        }
                        double tempo = track.getDouble("tempo");
                        Log.d("TEMPO", MetronomeService.this.bpm + " " + tempo);
                        MetronomeService.this.songTempo.add(Double.valueOf(tempo));
                        i++;
                    }
                }
                Log.d("BPMCOMPARATO", str4 + MetronomeService.this.bpm);
                if (MetronomeService.this.bpm < 80.0d) {
                    double unused = MetronomeService.this.bpm = MetronomeService.this.findMinBPM(MetronomeService.this.songTempo);
                    MetronomeService metronomeService = MetronomeService.this;
                    metronomeService.makeToast("BPM troppo basso, ora è stato impostato a " + MetronomeService.this.bpm + ". Arriverai in anticipo.");
                } else {
                    if (MetronomeService.this.bpm > MetronomeService.this.findMaxBPM(MetronomeService.this.songTempo)) {
                        this.canSpotifyPlay = false;
                        if (MetronomeService.this.mSpotifyAppRemote != null) {
                            MetronomeService.this.mSpotifyAppRemote.getPlayerApi().pause();
                        }
                        MetronomeService.this.metronomeBeep(MetronomeService.this.timeForBip);
                        MetronomeService.this.makeToast("BPM troppo alto, nessuna canzone in grado di raggiungerlo. Partirà il metronomo classico");
                    }
                }
                if (this.canSpotifyPlay) {
                    int i2 = 0;
                    while (true) {
                        if (i2 >= audioFeatures.length()) {
                            break;
                        } else if (i2 == audioFeatures.length() - 1) {
                            JSONObject jSONObject = result;
                            break;
                        } else {
                            JSONObject track2 = null;
                            if (audioFeatures.getJSONObject(i2) != null) {
                                track2 = audioFeatures.getJSONObject(i2);
                            }
                            StringBuilder sb = new StringBuilder();
                            JSONObject result2 = result;
                            sb.append(MetronomeService.this.bpm);
                            sb.append(" ");
                            double tempo2 = track2.getDouble("tempo");
                            sb.append(tempo2);
                            Log.d("TEMPO", sb.toString());
                            if (MetronomeService.this.bpm - 5.0d > tempo2 || MetronomeService.this.bpm + 5.0d < tempo2) {
                            } else {
                                int duration = track2.getInt("duration_ms");
                                double d = tempo2;
                                Log.d("ENTRATOCONNSPOTIFY", "ENTRATOOOOOO  " + i2);
                                MetronomeService.this.tracksToPlay.add(track2.getString("id"));
                                if (MetronomeService.this.tracksToPlay.size() > 40) {
                                    break;
                                }
                                Log.d("trackstoplaysize", str4 + MetronomeService.this.tracksToPlay.size());
                                MetronomeService.this.trackDuration.add(Integer.valueOf(duration));
                            }
                            i2++;
                            String str5 = s;
                            result = result2;
                        }
                    }
                    String str6 = "trackstoplaysize";
                    if (MetronomeService.this.tracksToPlay.size() > 0) {
                        MetronomeService.this.connectSpotify((String) MetronomeService.this.tracksToPlay.get(0));
                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                new JsonTaskQueue().execute(new Void[0]);
                            }
                        }, 2000);
                        return;
                    }
                    MetronomeService.this.makeToast("Nessuna canzone con questo bpm, arrotondato con la canzone con il bpm più alto subito dopo. Arriverai in anticipo");
                    double unused2 = MetronomeService.this.bpm = MetronomeService.this.findNearHighBPM(audioFeatures, MetronomeService.this.bpm);
                    Log.d("newBPM", str4 + MetronomeService.this.bpm);
                    int i3 = 0;
                    while (true) {
                        if (i3 >= audioFeatures.length()) {
                            break;
                        } else if (i3 == audioFeatures.length() - 1) {
                            JSONArray jSONArray = audioFeatures;
                            break;
                        } else {
                            JSONObject track3 = null;
                            if (audioFeatures.getJSONObject(i3) != null) {
                                track3 = audioFeatures.getJSONObject(i3);
                            }
                            double tempo3 = track3.getDouble("tempo");
                            StringBuilder sb2 = new StringBuilder();
                            JSONArray audioFeatures2 = audioFeatures;
                            String str7 = str6;
                            sb2.append(MetronomeService.this.bpm);
                            sb2.append(" ");
                            sb2.append(tempo3);
                            Log.d("TEMPO", sb2.toString());
                            if (MetronomeService.this.bpm - 5.0d > tempo3 || MetronomeService.this.bpm + 5.0d < tempo3) {
                                str2 = str7;
                                str = str4;
                            } else {
                                int duration2 = track3.getInt("duration_ms");
                                Log.d("ENTRATOCONNSPOTIFY", "ENTRATOOOOOO  " + i3);
                                MetronomeService.this.tracksToPlay.add(track3.getString("id"));
                                if (MetronomeService.this.tracksToPlay.size() > 40) {
                                    break;
                                }
                                str2 = str7;
                                Log.d(str2, str4 + MetronomeService.this.tracksToPlay.size());
                                str = str4;
                                MetronomeService.this.trackDuration.add(Integer.valueOf(duration2));
                            }
                            i3++;
                            str6 = str2;
                            audioFeatures = audioFeatures2;
                            str4 = str;
                        }
                    }
                    MetronomeService.this.connectSpotify((String) MetronomeService.this.tracksToPlay.get(0));
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            new JsonTaskQueue().execute(new Void[0]);
                        }
                    }, 2000);
                    return;
                }
                JSONArray jSONArray2 = audioFeatures;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class JsonTaskQueue extends AsyncTask<Void, Void, Void> {
        private JsonTaskQueue() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... voids) {
            int n;
            int length = MetronomeService.this.tracksToPlay.size();
            if (length < 5) {
                n = 8;
            } else if (length < 10) {
                n = 4;
            } else if (length < 20) {
                n = 2;
            } else {
                n = 1;
            }
            for (int k = 0; k < n; k++) {
                for (int i = 0; i < MetronomeService.this.tracksToPlay.size(); i++) {
                    MetronomeService.access$1508(MetronomeService.this);
                    Log.d("AGGIUNGOTRACKTROVATE", "add track to queue" + ((String) MetronomeService.this.tracksToPlay.get(i)));
                    MainActivity.actionJSONFromURLConnection("https://api.spotify.com/v1/me/player/queue?uri=spotify%3Atrack%3A" + ((String) MetronomeService.this.tracksToPlay.get(i)), "POST");
                }
            }
            return null;
        }
    }
}
