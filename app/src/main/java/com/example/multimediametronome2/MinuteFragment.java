package com.example.multimediametronome2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import java.util.Calendar;

public class MinuteFragment extends Fragment {
    /* access modifiers changed from: private */
    public MapsFragment mapsFragment;
    /* access modifiers changed from: private */
    public int minutes = 0;
    /* access modifiers changed from: private */
    public int seconds = 0;

    /* renamed from: sp */
    private SharedPreferences f64sp;
    private TimePicker timePicker;
    private View view;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_minute, container, false);
        this.f64sp = getActivity().getSharedPreferences("prefs", 0);
        ((TextView) this.view.findViewById(R.id.distanzaTW)).setText("Distanza: " + this.f64sp.getFloat("distancePolyline", 0.0f) + " m");
        final EditText minutesET = (EditText) this.view.findViewById(R.id.minutesEditText);
        final TimePicker timePicker2 = (TimePicker) this.view.findViewById(R.id.minutePicker);
        timePicker2.setIs24HourView(true);
        final FragmentManager fm = getActivity().getSupportFragmentManager();
        final RelativeLayout thisLayout = (RelativeLayout) this.view.findViewById(R.id.layoutMinuteFragment);
        this.mapsFragment = (MapsFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        ((Button) this.view.findViewById(R.id.buttonStartRoute)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String minInput = minutesET.getText().toString();
                Log.d("MINUTES", "minInput: " + minInput + " minutes: " + String.valueOf(MinuteFragment.this.minutes));
                if (!minInput.equals("")) {
                    MinuteFragment.this.mapsFragment.setImageStopToStartRoute();
                    int unused = MinuteFragment.this.minutes = Integer.parseInt(minInput);
                    Log.d("ENTRATO", String.valueOf(MinuteFragment.this.minutes));
                    MinuteFragment minuteFragment = MinuteFragment.this;
                    int unused2 = minuteFragment.seconds = minuteFragment.seconds + (MinuteFragment.this.minutes * 60);
                    MinuteFragment.this.startServiceMetronome();
                    Log.d("SECONDSINPUT", "" + MinuteFragment.this.seconds);
                } else {
                    Calendar rightNow = Calendar.getInstance();
                    int differenceInMinutes = ((timePicker2.getHour() * 60) + timePicker2.getMinute()) - ((rightNow.get(11) * 60) + rightNow.get(12));
                    if (differenceInMinutes <= 0) {
                        Toast.makeText(MinuteFragment.this.getContext(), "L'ora inserita deve essere maggiore di quella attuale", 0).show();
                    } else {
                        MinuteFragment.this.mapsFragment.setImageStopToStartRoute();
                        MinuteFragment minuteFragment2 = MinuteFragment.this;
                        int unused3 = minuteFragment2.seconds = minuteFragment2.seconds + (differenceInMinutes * 60);
                        MinuteFragment.this.startServiceMetronome();
                        Log.d("SECONDSPICKER", "" + MinuteFragment.this.seconds);
                    }
                }
                Log.d("FINALSECONDS", String.valueOf(MinuteFragment.this.seconds));
                fm.popBackStack();
            }
        });
        ((Button) this.view.findViewById(R.id.buttonExitStartRoute)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                fm.popBackStack();
                Log.d("EXITBUTTON", "click on exit");
                Log.d("FRAMEVISIBILITY", String.valueOf(thisLayout.getVisibility()));
            }
        });
        return this.view;
    }

    /* access modifiers changed from: private */
    public void startServiceMetronome() {
        Intent startMetronome = new Intent(getActivity(), MetronomeService.class);
        startMetronome.putExtra("seconds", this.seconds);
        getActivity().startService(startMetronome);
    }
}
