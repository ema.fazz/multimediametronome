package com.example.multimediametronome2.directionhelpers;

public interface TaskLoadedCallback {
    void onTaskDone(Object... objArr);
}
