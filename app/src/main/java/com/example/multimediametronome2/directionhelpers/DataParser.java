package com.example.multimediametronome2.directionhelpers;

import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DataParser {
    public List<List<HashMap<String, String>>> parse(JSONObject jObject) {
        List<List<HashMap<String, String>>> routes = new ArrayList<>();
        try {
            JSONArray jRoutes = jObject.getJSONArray("routes");
            for (int i = 0; i < jRoutes.length(); i++) {
                JSONArray jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
                List path = new ArrayList();
                for (int j = 0; j < jLegs.length(); j++) {
                    JSONArray jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");
                    for (int k = 0; k < jSteps.length(); k++) {
                        String polyline = (String) ((JSONObject) ((JSONObject) jSteps.get(k)).get("polyline")).get("points");
                        List<LatLng> list = decodePoly(polyline);
                        int l = 0;
                        while (l < list.size()) {
                            HashMap<String, String> hm = new HashMap<>();
                            hm.put("lat", Double.toString(list.get(l).latitude));
                            hm.put("lng", Double.toString(list.get(l).longitude));
                            path.add(hm);
                            l++;
                            polyline = polyline;
                        }
                    }
                    routes.add(path);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e2) {
        }
        return routes;
    }

    private List<LatLng> decodePoly(String encoded) {
        int index;
        int result;
        int b;
        String str = encoded;
        List<LatLng> poly = new ArrayList<>();
        int b2 = 0;
        int lat = 0;
        int lng = 0;
        for (int len = encoded.length(); b2 < len; len = len) {
            int shift = 0;
            int result2 = 0;
            while (true) {
                index = b2 + 1;
                int b3 = str.charAt(b2) - 63;
                result2 |= (b3 & 31) << shift;
                shift += 5;
                if (b3 < 32) {
                    break;
                }
                b2 = index;
            }
            int lat2 = lat + ((result2 & 1) != 0 ? ~(result2 >> 1) : result2 >> 1);
            int shift2 = 0;
            int result3 = 0;
            while (true) {
                result = index + 1;
                b = str.charAt(index) - 63;
                result3 |= (b & 31) << shift2;
                shift2 += 5;
                if (b < 32) {
                    break;
                }
                int i = b;
                int i2 = len;
                index = result;
            }
            lng += (result3 & 1) != 0 ? ~(result3 >> 1) : result3 >> 1;
            int i3 = b;
            poly.add(new LatLng(((double) lat2) / 100000.0d, ((double) lng) / 100000.0d));
            b2 = result;
            lat = lat2;
        }
        return poly;
    }
}
