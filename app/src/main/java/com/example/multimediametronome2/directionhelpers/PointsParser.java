package com.example.multimediametronome2.directionhelpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONObject;

public class PointsParser extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
    Context context;
    String directionMode = "driving";
    TaskLoadedCallback taskCallback;

    public PointsParser(Context mContext, String directionMode2) {
        this.taskCallback = (TaskLoadedCallback) mContext;
        this.directionMode = directionMode2;
        this.context = mContext;
    }

    /* access modifiers changed from: protected */
    public List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
        List<List<HashMap<String, String>>> routes = null;
        try {
            JSONObject jObject = new JSONObject(jsonData[0]);
            Log.d("mylog", jsonData[0].toString());
            DataParser parser = new DataParser();
            Log.d("mylog", parser.toString());
            routes = parser.parse(jObject);
            Log.d("mylog", "Executing routes");
            Log.d("mylog", routes.toString());
            return routes;
        } catch (Exception e) {
            Log.d("mylog", e.toString());
            e.printStackTrace();
            return routes;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(List<List<HashMap<String, String>>> result) {
        List<HashMap<String, String>> path;
        SharedPreferences prefs = this.context.getSharedPreferences("prefs", 0);
        SharedPreferences.Editor edit = prefs.edit();
        float totalDistance = 0.0f;
        PolylineOptions lineOptions = null;
        for (int i = 0; i < result.size(); i++) {
            ArrayList<LatLng> points = new ArrayList<>();
            lineOptions = new PolylineOptions();
            List<HashMap<String, String>> path2 = result.get(i);
            int j = 0;
            while (j < path2.size()) {
                HashMap<String, String> point = path2.get(j);
                double lat = Double.parseDouble(point.get("lat"));
                float totalDistance2 = totalDistance;
                double lng = Double.parseDouble(point.get("lng"));
                points.add(new LatLng(lat, lng));
                if (j > 0) {
                    double d = lng;
                    Location start = new Location("");
                    Location end = new Location("");
                    path = path2;
                    start.setLatitude(points.get(j - 1).latitude);
                    start.setLongitude(points.get(j - 1).longitude);
                    end.setLatitude(points.get(j).latitude);
                    end.setLongitude(points.get(j).longitude);
                    float totalDistance3 = totalDistance2 + start.distanceTo(end);
                    Location location = start;
                    Log.d("DISTANCENOW", String.valueOf(totalDistance3));
                    totalDistance = totalDistance3;
                } else {
                    path = path2;
                    totalDistance = totalDistance2;
                }
                j++;
                List<List<HashMap<String, String>>> list = result;
                path2 = path;
            }
            float f = totalDistance;
            List<HashMap<String, String>> list2 = path2;
            edit.putFloat("distancePolyline", totalDistance);
            edit.apply();
            Log.d("SHAREDPREFDISTANCE", String.valueOf(prefs.getFloat("distancePolyline", 0.0f)));
            lineOptions.addAll(points);
            if (this.directionMode.equalsIgnoreCase("walking")) {
                lineOptions.width(10.0f);
                lineOptions.color(-65281);
            } else {
                lineOptions.width(20.0f);
                lineOptions.color(-16776961);
            }
            Log.d("mylog", "onPostExecute lineoptions decoded");
        }
        if (lineOptions != null) {
            this.taskCallback.onTaskDone(lineOptions);
            return;
        }
        Log.d("mylog", "without Polylines drawn");
    }
}
