package com.example.multimediametronome2;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import com.example.multimediametronome2.database.StepsLengthHistory;
import java.util.List;

public class DatiDelleCorseFragment extends Fragment {
    /* access modifiers changed from: private */
    public TextView datiTW;
    private View view;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.fragment_dati_delle_corse, container, false);
        this.view = inflate;
        this.datiTW = (TextView) inflate.findViewById(R.id.datiTextView);
        new ShowSLHistoryTask().execute(new Void[0]);
        return this.view;
    }

    private class ShowSLHistoryTask extends AsyncTask<Void, Void, Void> {
        List<StepsLengthHistory> shList;

        private ShowSLHistoryTask() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... voids) {
            this.shList = MainActivity.mydb.mydaoSH().getSLH();
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            String dati = "";
            for (int i = 0; i < this.shList.size(); i++) {
                dati = dati + "ID: " + this.shList.get(i).getId() + " StepLen: " + this.shList.get(i).getStepLength() + " SD: " + this.shList.get(i).getStandardDeviation() + " Date: " + this.shList.get(i).getRegistrationDate() + "\n\n";
            }
            DatiDelleCorseFragment.this.datiTW.setText(dati);
        }
    }
}
